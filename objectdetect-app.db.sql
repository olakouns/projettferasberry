BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "missions" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"distance"	REAL,
	"nbrobjdetect"	INTEGER,
	"duree"	TEXT
);
CREATE TABLE IF NOT EXISTS "objectdetect" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"distance"	REAL,
	"typeofobject"	TEXT,
	"time"	INTEGER,
	"id_mission"	INTEGER,
	FOREIGN KEY("id_mission") REFERENCES "missions"("id")
);
COMMIT;
