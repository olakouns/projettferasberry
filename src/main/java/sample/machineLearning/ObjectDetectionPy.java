package sample.machineLearning;

import sample.controller.CameraController;
import sample.controller.DashbordController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ObjectDetectionPy {
    public synchronized static Map<String, Integer> detectObject(String path, int i){
        DashbordController dashbordController = new DashbordController();
         String s = "";
        System.out.println(path);
        System.out.println(i);
                try {
                    Process p = Runtime.getRuntime().exec("python F:\\yolo-object-detection\\yolofinal.py  --folder F:\\yolo-object-detection\\objects_detects\\"+i+" --image "+path+" --yolo F:\\yolo-object-detection\\yolo-coco");
                    BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    while ((s = in.readLine()) != null){
                        System.out.println(s);

                        return refactor(s);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
          return refactor(s);
    }
    private static Map<String, Integer> refactor(String value){
        HashMap<String, Integer> map = new HashMap<>();
        if (value.isEmpty()){
            System.out.println("empty");
        }else {
            List<String> arrayList =  Arrays.asList(value.substring(1,(value.length()-1)).replaceAll("\\s","").split(","));
            arrayList.forEach(item -> {
                if (!item.isEmpty()){
                    String[] inter = item.substring(1,(item.length()-1)).split(":");
                    //System.out.println(inter.length);

                    map.put(inter[0], Integer.parseInt(inter[1]));
                }
            });
        }
        return map;
    }
}
