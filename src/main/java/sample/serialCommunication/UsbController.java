package sample.serialCommunication;


import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import java.io.BufferedReader;
import java.io.IOException;

public class UsbController  {
    private SerialPort comPort ;
    ///SerialPort comPort = SerialPort.getCommPorts()[0];
    private static UsbController listener = new UsbController();
    private BufferedReader input;
    private Usbcallback usbcallback ;
    private String inputLine="";
    private String datareceiver="";

    private UsbController() {
        //System.out.println(SerialPort.getCommPorts()[0]);
       // input = new BufferedReader(new InputStreamReader(comPort.getInputStream()));
    }
    public void initilizer(){
        if (SerialPort.getCommPorts().length == 0){
            System.out.println("Com port non trouvé");
            return;
        }
        comPort = SerialPort.getCommPorts()[0];
        comPort.openPort();

    }

    public static UsbController getInstance() {
        return listener;
    }

    public void setUsbcallback(Usbcallback usbcallback) {
        this.usbcallback = usbcallback;
    }

    public void read(){
        if (comPort == null){
            System.out.println("impossible de lire les donnees");
        }else {
            comPort.addDataListener(new SerialPortDataListener() {
                @Override
                public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_RECEIVED; }
                @Override
                public void serialEvent(SerialPortEvent serialPortEvent) {
                    byte[] newData = serialPortEvent.getReceivedData();

                    for (int i = 0; i < newData.length; ++i){
                        inputLine = inputLine + Character.toString((char)newData[i]);
                        //System.out.print((char)newData[i]);
                        if (newData[i] == '\n'){
                            //System.out.println(inputLine);
                            if (inputLine != null){
                                datareceiver = inputLine;
                                if (usbcallback != null) {
                                    try {
                                        usbcallback.receiveData(inputLine);
                                        Thread.sleep(2000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                inputLine = "";
                            }
                        }
                    }
                }
            });
        }
    }

    public void send(String data){
        if (comPort == null){
            System.out.println("impossible d'envoyer les donnees");
        }else {
            try {
                comPort.getOutputStream().write(data.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void close(){
        if (comPort == null){
            System.out.println("Le port est toujours ferme");
        }else {
            comPort.closePort();
        }
    }
    public  String receivedata(){
        if (comPort == null){
            System.out.println("impossible de recevoir la donnee");
        }else {
            return datareceiver;
        }
        return "";
    }

}
//    private SerialPort serialPort;
//    private final String namePort;
//    private Usbcallback usbcallback ;
//
//    public UsbController(String portName) {
//        this.namePort = portName;
//    }
//
//    private BufferedReader input;
//    private InputStream inputStream;
//
//    private OutputStream output;
//    /**
//     * Milliseconds to block while waiting for port open
//     */
//    private static final int TIME_OUT = 2000;
//    /**
//     * Default bits per second for COM port.
//     */
//    private static final int DATA_RATE = 9600;
//
//    private String inputLine;
//
//    public void initialize() {
//        CommPortIdentifier portId = null;
//        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
//
//        //First, Find an instance of serial port as set in PORT_NAMES.
//        while (portEnum.hasMoreElements()) {
//            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
//            System.out.println(currPortId.getName());
//            if (namePort.equals(currPortId.getName())) {
//                portId = currPortId;
//                break;
//            }
//        }
//        if (portId == null) {
//            System.out.println("Could not find COM port.");
//            return;
//        }
//
//        try {
//            // open serial port, and use class name for the appName.
//            serialPort = (SerialPort) portId.open(this.getClass().getName(),
//                    TIME_OUT);
//            System.out.println("Connect to USB");
//            // set port parameters
//            serialPort.setSerialPortParams(DATA_RATE,
//                    SerialPort.DATABITS_8,
//                    SerialPort.STOPBITS_1,
//                    SerialPort.PARITY_NONE);
//            System.out.println("Init conf");
//            // open the streams
//            inputStream = serialPort.getInputStream();
//            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
//            output = serialPort.getOutputStream();
//
//            // add event listeners
//            serialPort.addEventListener(this);
//            System.out.println("Listener done!");
//            serialPort.notifyOnDataAvailable(true);
//            System.out.println("data available");
//        } catch (PortInUseException | UnsupportedCommOperationException | IOException | TooManyListenersException e) {
//            e.printStackTrace();
//            System.err.println(e.toString());
//        }
//    }
//
//    public void send(String data) {
//        try {
//            output.write(data.getBytes());
//        } catch (Exception e) {
//            System.err.println(e.toString());
//        }
//    }
//
//    public void setUsbcallback(Usbcallback usbcallback) {
//        this.usbcallback = usbcallback;
//    }
//
//    public String read() {
//        return inputLine;
//    }
//
//    public void sleep(int time) {
//        try {
//            Thread.sleep(time);
//        } catch (InterruptedException e) {
//            System.err.println(e.toString());
//        }
//    }
//
//    /**
//     * This should be called when you stop using the port. This will prevent
//     * port locking on platforms like Linux.
//     */
//    public synchronized void close() {
//        if (serialPort != null) {
//            serialPort.removeEventListener();
//            serialPort.close();
//        }
//    }
//
//    /**
//     * Handle an event on the serial port. Read the data and print it.
//     *
//     */
//    @Override
//    public synchronized void serialEvent(SerialPortEvent oEvent) {
////        System.out.println("event = " + oEvent.getEventType());
////        System.out.println("event input = " + inputStream);
//        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
//
//            try {
//                //System.out.println("ok");
//                inputLine=input.readLine();
//                // System.out.println(inputLine);
//                if (inputLine != null){
//                    usbcallback.receiveData(inputLine);
//                }
//                //System.out.println(inputLine);
//            } catch (Exception e) {
//                System.err.println(e.toString());
//                System.out.println("ok");
//            }
//        }
//    }