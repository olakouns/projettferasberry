package sample.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class StatistiquesController implements Initializable {
    public TableView<Map.Entry<String,String>> tableview ;
    public TableColumn<Map.Entry<String, String>, String> objecttype ;
    public TableColumn<Map.Entry<String, String>,String> nombre;
    public Label tot;
    private  Map<String,String> map = new HashMap<>();
    private Integer total = 0;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setmap();
        objecttype.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, String>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<String, String>, String> p) {
                return new SimpleStringProperty(p.getValue().getKey());
            }
        });
        nombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, String>, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<String, String>, String> p) {
                return new SimpleStringProperty(p.getValue().getValue());
            }
        });
        ObservableList<Map.Entry<String, String>> items = FXCollections.observableArrayList(map.entrySet());
        tableview.setItems(items);
        tableview.getColumns().setAll(objecttype, nombre);
        tot.setText(String.valueOf(total));
    }

    private void setmap(){
        System.out.println("inter statistique windows");
        DashbordController.maptab.forEach((s, integer) -> {
            map.put(s,String.valueOf(integer));
            total = total + integer;
        });
       // map.put("Total",String.valueOf(total));
        System.out.println(map);
    }
}
