package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.dao.DBManangerImpl;
import sample.entities.Mission;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryitemController {
    public Label labelDate;
    public Label labelHours;
    public Label labelDistance;
    public Label labelNbrdetecte;
    public AnchorPane parent;
    private  Mission mis;
    public  int  id ;

    public void setData(Mission mission){
        this.mis = mission;
        id = mis.getId();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        labelDistance.setText(String.valueOf(mission.getDistance())+" m");
        labelNbrdetecte.setText(String.valueOf(mission.getNbrobjectdetect()));
        //System.out.println(mission.getDuree());
        labelDate.setText(mission.getDuree().split(" ")[0]);
        labelHours.setText(mission.getDuree().split(" ")[1]);
        //new Date(mission.getDuree())
    }

    public void deletemission(ActionEvent actionEvent) throws SQLException {
        DBManangerImpl.getInstance().delteMission(mis.getId());
        parent.setVisible(false);
        parent.managedProperty().bind(parent.visibleProperty());
    }

    public void seeplus(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/gallery.fxml"));
        Parent root = fxmlLoader.load();
        GalleryController galleryController = fxmlLoader.getController();
        galleryController.receivedata(id);
        Stage primaryStage = new Stage();
        primaryStage.setTitle("details de la mission" + id);
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 510, 661));
        primaryStage.show();
    }
}
