package sample.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import nu.pattern.OpenCV;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class CameraController implements Initializable {
    @FXML
    public ImageView currentframe;
    private Timer timer;
    private VideoCapture capture ;
    private boolean cameraActive;
    private Mat frametosnap;
    private  TimerTask frameGrabber;
    private static  boolean isRun = true;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        OpenCV.loadLocally();
        this.capture = new VideoCapture();
    }

    public void startCamera(){
        currentframe.setPreserveRatio(true);
            this.capture.open(0);
            if (this.capture.isOpened()) {
                this.cameraActive = true;
                frameGrabber = new TimerTask() {
                    @Override
                    public void run()
                    {
                        if(isRun){
                            Image tmp = grabFrame();
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run()
                                {
                                    currentframe.setImage(tmp);
                                    currentframe.setFitHeight(375);
                                }
                            });

                        }

                    }
                };
                isRun = true;
                this.timer = new Timer();
                //set the timer scheduling, this allow you to perform frameGrabber every 33ms;
                this.timer.schedule(frameGrabber, 0, 200);
            }
    }
    public void paused(){
       isRun  = false;

    }
    public void play(){
        isRun = true;
    }
    public void stopCamera(){
        if (this.timer != null){
            this.timer.cancel();
            this.timer = null;
        }
        // release the camera
        this.capture.release();
        // clear the image container
        currentframe.setImage(null);
        System.out.println("stop successful");
    }

    public String takesanpshut(){
        String filesave = "F:\\"+generateString()+".jpg";
        //String filesave = "F:/ok.jpg";
        //Writing the image
        Imgcodecs.imwrite(filesave, frametosnap);
        System.out.println("Image Saved");
        return  filesave;
    }

    private String generateString(){
        Random rand = new Random();
        String str="";
        for(int i = 0 ; i < 20 ; i++){
            char c = (char)(rand.nextInt(26) + 97);
            str += c;
        }
        return str;

    }

    private Image grabFrame(){
        //init
        Image imageToShow = null;
        Mat frame = new Mat();
        // check if the capture is open
        if (this.capture.isOpened())
        {
            try
            {
                // read the current frame
                this.capture.read(frame);
                // if the frame is not empty, process it
                if (!frame.empty())
                {
                    // convert the Mat object (OpenCV) to Image (JavaFX)
                    imageToShow = mat2Image(frame);
                    frametosnap = frame;
                }
            }
            catch (Exception e)
            {
                // log the error
                System.err.println("ERROR: " + e.getMessage());
            }
        }
        return imageToShow;
    }

    private Image mat2Image(Mat frame){
        // create a temporary buffer
        MatOfByte buffer = new MatOfByte();
        // encode the frame in the buffer
        Imgcodecs.imencode(".png", frame, buffer);
        // build and return an Image created from the image encoded in the buffer
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }
}
