package sample.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import sample.dao.DBManangerImpl;
import sample.entities.Mission;
import sample.serialCommunication.UsbController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML
    public ImageView imgeview;
    @FXML
    public Circle indicador1;
    @FXML
    public Circle indicador2;
    @FXML
    public Circle indicador3;
    public Timeline fiveSecondsWonder;
    public VBox vboxhistory;
    int indexslide = 0;
    String images[] = new String[] {"slidefirst.jpg","slideseconde.jpg","slide.jpg"};
    DBManangerImpl dbMananger = DBManangerImpl.getInstance();
    List<Mission> missions = new ArrayList<Mission>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(3), event -> {
            slide();
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
        try {
            missions = dbMananger.getAllMission();
            System.out.println(missions.size());
            //System.out.println("ok");
            for (Mission mission: missions) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                try {
                    AnchorPane pane = fxmlLoader.load(DashbordController.class.getResourceAsStream("/views/historyitem.fxml"));
                    pane.getStylesheets().add(getClass().getResource("/css/itemhistory.css").toString());
                    HistoryitemController historyitemController = fxmlLoader.getController();
                    historyitemController.setData(mission);
                    vboxhistory.getChildren().add(pane);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void startmission(ActionEvent actionEvent) throws IOException {
        fiveSecondsWonder.stop();
        Parent root = FXMLLoader.load(getClass().getResource("/views/dashbord.fxml"));
        Stage primaryStage = new Stage();
        //primaryStage.getIcons().add(new Image("/images/play.png"));
        primaryStage.setTitle("Tableau de bord");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 770, 661));
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            DashbordController.cameraController.stopCamera();
            //DashbordController.fiveSecondsWonder.stop();
            if (DashbordController.openPort)
                UsbController.getInstance().close();
        });
    }



    private void slide(){
        if (indexslide == images.length){
            indexslide = 0;
        }
        System.out.println(indexslide);
        if(indexslide == 0){
            indicador1.setFill(Paint.valueOf("#563f9e"));
            indicador2.setFill(Paint.valueOf("#ededed"));
            indicador3.setFill(Paint.valueOf("#ededed"));
        }
        if(indexslide == 1){
            indicador1.setFill(Paint.valueOf("#ededed"));
            indicador2.setFill(Paint.valueOf("#563f9e"));
            indicador3.setFill(Paint.valueOf("#ededed"));
        }
        if(indexslide == 2){
            indicador1.setFill(Paint.valueOf("#ededed"));
            indicador2.setFill(Paint.valueOf("#ededed"));
            indicador3.setFill(Paint.valueOf("#563f9e"));
        }


        try {
            String imagepah = getClass().getResource("/images/" + images[indexslide]).toURI().toString();
            System.out.println(imagepah);
            Image image = new Image(imagepah);
            Platform.runLater(() -> {
                imgeview.setImage(image);
            });
        }catch (URISyntaxException  e){

        }
        indexslide ++ ;
    }

    public void refresh(ActionEvent actionEvent) {
        List<Mission> missionList = new ArrayList<>();
        try {
            missionList = dbMananger.getAllMission();
            if (missionList.size() != missions.size()){
                for (int i = missions.size()+1; i<=missionList.size(); i++){
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    try {
                        AnchorPane pane = fxmlLoader.load(DashbordController.class.getResourceAsStream("/views/historyitem.fxml"));
                        pane.getStylesheets().add(getClass().getResource("/css/itemhistory.css").toString());
                        HistoryitemController historyitemController = fxmlLoader.getController();
                        historyitemController.setData(missionList.get(i-1));
                        vboxhistory.getChildren().add(0, pane);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                System.out.println("already to update");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

