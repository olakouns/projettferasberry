package sample.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import sample.dao.DBManangerImpl;
import sample.entities.DataReceive;
import sample.entities.Mission;
import sample.entities.ObjectDetect;
import sample.machineLearning.ObjectDetectionPy;
import sample.metiers.ManagerMetier;
import sample.mode.Automode;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public  class DashbordController implements Initializable {
    @FXML
    public VBox vbox;
    public HBox hboxhome;
    public VBox vboxhistory = new VBox();
    public TextField distanceinput;
    public Button autobtnstop;
    public Button autobtnstart;
    public AnchorPane autocmd;
    public AnchorPane manuelcmd;
    public ToggleButton btnautomode;
    public ToggleButton btnmanuelmode;
    public Button stopmanuel;
    public Button startbtnauto;
    public ScrollPane scrollPane;
    private FXMLLoader fxmlLoader = new FXMLLoader();
    public  static  CameraController cameraController;
    public static Timeline fiveSecondsWonder;
    public   ManagerMetier managerMetier;
    public static  boolean openPort = false;
    public static Map<String,Integer> maptab = new HashMap<String,Integer>();
    private Date datestart ;
    private Date dateend;
    private Integer nbrtotal = 0;
    private DBManangerImpl dbMananger = DBManangerImpl.getInstance();
    private List<ObjectDetect> objectDetects ;
    private Gson gson = new Gson();
    private DataReceive dataReceive = new DataReceive();
    public static Mission mission;
    private static boolean inprocess = true;
    private  int mode;
    public static DashbordController dashbordController ;
    Thread thread ;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        camerastart();
        ToggleGroup group = new ToggleGroup();
        btnautomode.setToggleGroup(group);
        btnmanuelmode.setToggleGroup(group);
        autobtnstop.setDisable(true);

        manuelcmd.setVisible(false);
        manuelcmd.managedProperty().bind(manuelcmd.visibleProperty());
        autocmd.setVisible(true);
        stopmanuel.setDisable(true);
        dashbordController = new DashbordController();
    }


    // start camera
    private void camerastart(){
        try {
            AnchorPane anchorPane = fxmlLoader.load(getClass().getResourceAsStream("/views/camera.fxml"));
            cameraController = fxmlLoader.getController();
            vbox.getChildren().add(anchorPane);
            System.out.println(cameraController);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e);
        }
        cameraController.startCamera();
        //cameraController.play();
    }


    private  void defaultmode(){
        thread = new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                    while (inprocess){
                        Date date = new Date();
                        try{
                           dataReceive = gson.fromJson(ManagerMetier.getInstance().usbController.receivedata(), DataReceive.class);

                        }catch (NumberFormatException | JsonSyntaxException e){
                            e.printStackTrace();
                        }
                        System.out.println(dataReceive);
                        if (dataReceive != null){
                            System.out.println("take a snashut");
                            String path = cameraController.takesanpshut();
                            //cameraController.paused();
                            HashMap<String,Integer> map = (HashMap<String,Integer>) ObjectDetectionPy.detectObject(path,mission.getId());
                            System.out.println(map);
                            map.forEach((type, nbre) -> {
                                Platform.runLater(() -> {
                                    loadashbordItem(type,dataReceive.getUltfront(),nbre,date);
                                    //cameraController.play();
                                });

                            });
                            System.out.println("ok");
                            setmap(map);
                        }else {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }

            }
        });
        thread.start();

//        fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(5), event -> {
//        }));
//        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
    }

    // fonctionnement automatique
    @FXML
    public void automatique(ActionEvent actionEvent) {
        manuelcmd.setVisible(false);
        manuelcmd.managedProperty().bind(manuelcmd.visibleProperty());
        autocmd.setVisible(true);
        hboxhome.setFocusTraversable(true);
    }

    public void startautomode(ActionEvent actionEvent) {
        if (!distanceinput.getText().isEmpty()){
            try {
                dbMananger.insertMission(new Mission(0,0,""));
                mission = dbMananger.lassMission();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            datestart = new Date();
//            fiveSecondsWonder.play();
            inprocess = true;
            defaultmode();
            nbrtotal = 0;
            objectDetects = new ArrayList<ObjectDetect>();
             autobtnstart.setText("Continuer");
             ManagerMetier.getInstance().usbController.setUsbcallback(new Automode(Float.parseFloat(distanceinput.getText())));
             openPort = true;
            btnmanuelmode.setDisable(true);
            autobtnstop.setDisable(false);
            btnmanuelmode.setDisable(true);
        }else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Avertissement");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez entrer une distance correcte!");
            alert.showAndWait();
        }
    }

    public void stopautomode(ActionEvent actionEvent) {
        inprocess = false;
        thread.interrupt();
        btnmanuelmode.setDisable(false);
        autobtnstop.setDisable(true);
        ManagerMetier.getInstance().usbController.setUsbcallback(null);
        System.out.println("arret de reception");
        btnmanuelmode.setDisable(false);
        dateend = new Date();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            mission.setDuree(dateFormat.format(new Date())+" "+ formatdate(dateend.getTime()-datestart.getTime()));
            mission.setDistance(Double.parseDouble(distanceinput.getText()));
            mission.setNbrobjectdetect(nbrtotal);
            mission = dbMananger.updateMission(mission);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    //fonctionnement manuel
    @FXML
    public void manuel(ActionEvent actionEvent) {
        manuelcmd.setVisible(true);
        autocmd.setVisible(false);
        autocmd.managedProperty().bind(autocmd.visibleProperty());

    }

    public void startmanuelmode(ActionEvent actionEvent) throws SQLException {
        datestart = new Date();
        dbMananger.insertMission(new Mission(0,0,""));
        mission = dbMananger.lassMission();
        inprocess = true;
        defaultmode();

        nbrtotal = 0;
        objectDetects = new ArrayList<ObjectDetect>();
        startbtnauto.setText("Continuer");
        stopmanuel.setDisable(false);
        hboxhome.setFocusTraversable(true);
        btnautomode.setDisable(true);
        manuelmode();

    }

    public void stopmanuelmode(ActionEvent actionEvent) {
        inprocess = false;
        thread.interrupt();
        System.out.println("stop");
        startbtnauto.setText("Demarrer");
        btnautomode.setDisable(false);
        autocmd.setDisable(false);
        stopmanuel.setDisable(true);
        btnautomode.setDisable(false);
        hboxhome.setOnKeyReleased(keyEvent -> {
        });
        System.out.println(maptab);
        dateend = new Date();
        //System.out.println(dateend.getTime()-datestart.getTime());
        //passer les valeurs de la missions a la fin et faire un update
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            mission.setDuree(dateFormat.format(new Date())+" "+ formatdate(dateend.getTime()-datestart.getTime()));
            System.out.println(calculatedistance(dateend.getTime()-datestart.getTime()));
            mission.setDistance(calculatedistance(dateend.getTime()-datestart.getTime()));
            mission.setNbrobjectdetect(nbrtotal);
             mission = dbMananger.updateMission(mission);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void manuelmode(){
        hboxhome.setOnKeyPressed(keyEvent -> {
            System.out.printf("Touche relache : %s", keyEvent.getCode(), keyEvent.getCharacter()).println();
        });

        hboxhome.setOnKeyReleased(keyEvent -> {
            System.out.printf("Touche relache : %s", keyEvent.getCode(), keyEvent.getCharacter()).println();
            try {
                Thread.sleep(500);
                System.out.printf("Touche enfoncée : %s", keyEvent.getCode(), keyEvent.getCharacter()).println();
                if (keyEvent.getCode() == KeyCode.NUMPAD8){
                    System.out.println("Go forword");
                    ManagerMetier.getInstance().goForward();
                    Thread.sleep(2000);
                }
                if (keyEvent.getCode() == KeyCode.NUMPAD6){
                    System.out.println("Turn right");
                    ManagerMetier.getInstance().turnRight();
                    Thread.sleep(2000);
                }
                if (keyEvent.getCode() == KeyCode.NUMPAD4){
                    System.out.println("Turn left");
                    ManagerMetier.getInstance().turnLeft();
                    Thread.sleep(2000);
                }
                if (keyEvent.getCode() == KeyCode.NUMPAD2){
                    System.out.println("Go back");
                    ManagerMetier.getInstance().goBack();
                    Thread.sleep(2000);
                }if (keyEvent.getCode() == KeyCode.NUMPAD5){
                    System.out.println("Stop");
                    ManagerMetier.getInstance().stop();
                    Thread.sleep(2000);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    // remttre le compteur a zero
    public void resetmode(ActionEvent actionEvent) {

    }

    public void seestartistique(ActionEvent actionEvent) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/statistiques.fxml"));
            StatistiquesController statistiquesController = fxmlLoader.getController();
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Statistiques");
            primaryStage.setResizable(false);
            primaryStage.setScene(new Scene(root, 500, 500));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public  void loadashbordItem(String type, double distance, int nbr, Date datedetect){
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            //objectDetects.add(new ObjectDetect(distance,type, (int) datedetect.getTime()));
            dbMananger.insertObjectDetect(mission.getId(),new ObjectDetect(distance,type, (int) datedetect.getTime()));
            AnchorPane pane = fxmlLoader.load(DashbordController.class.getResourceAsStream("/views/dashborditem.fxml"));
            pane.getStylesheets().add(getClass().getResource("/css/dashborditem.css").toString());
            DashborditemController dashborditemController = fxmlLoader.getController();
            dashborditemController.setData(type, distance, nbr, datedetect);
            vboxhistory.getChildren().add(0, pane);

            System.out.println(vboxhistory.getChildren());
            //scrollPane.setVvalue(1.0);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void setmap(HashMap<String,Integer> map) {
       // System.out.println(map);
            Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String,Integer> mapentry = iterator.next();
                if (maptab.containsKey(mapentry.getKey())){
                    System.out.println("inter");
                    maptab.forEach((s, integer) -> {
                        System.out.println("inter in foreach");
                        if (mapentry.getKey().equals(s)){
                            System.out.println("inter if statement");
                            int a = integer;
                            System.out.println("put add");
                            maptab.put(s,a+mapentry.getValue());
                            nbrtotal = nbrtotal + mapentry.getValue();
                        }
                    });
                }else {
                    System.out.println("put");
                    maptab.put(mapentry.getKey(),mapentry.getValue());
                    nbrtotal = nbrtotal + mapentry.getValue();
                }
            }
    }

    private String formatdate(long different){
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different/hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different/minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different/secondsInMilli;

        return String.valueOf(elapsedHours)+":"+String.valueOf(elapsedMinutes)+":"+String.valueOf(elapsedSeconds);
    }

    private double calculatedistance(double milis){

        return  (double) Math.round((milis/7000.0) * 100) / 100;
    }
}
