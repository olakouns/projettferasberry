package sample.controller;

import javafx.scene.control.Label;

import java.text.DecimalFormat;
import java.util.Date;


public class DashborditemController {
    public Label labelDistance;
    public Label time;
    public Label labeltype;
    public Label labelNbrdetecte;
    private DecimalFormat f = new DecimalFormat();
    public void setData(String type , double distance, int nbr, Date times){
        f.setMaximumFractionDigits(2);
        labelDistance.setText(f.format(distance)+'m');
        labelNbrdetecte.setText(String.valueOf(nbr));
        labeltype.setText(type);
        time.setText(String.valueOf(times.getHours())+':'+String.valueOf(times.getMinutes())+':'+String.valueOf(times.getSeconds()));
    }
}
