package sample.controller;

import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

public class GalleryController implements Initializable {
    public TilePane tile;
    private int id;
    private String path = "";


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public void receivedata(int id){
        int i = 1;
        path = "F:\\yolo-object-detection\\objects_detects\\"+id+"\\";
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null){
            for (final File file : listOfFiles) {
                ImageView imageView;
                imageView = createImageView(file,i);
                i++;
                tile.getChildren().addAll(imageView);
            }
        }else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Avertissement");
            alert.setHeaderText(null);
            alert.setContentText("Aucun objects n'a été detectés au cours de la mission "+ id +" !");
            alert.showAndWait();
        }
    }
    private ImageView createImageView(File imageFile, int i) {
        ImageView imageView = null;
        try {
            final Image image = new Image(new FileInputStream(imageFile), 150, 0, true,
                    true);
            imageView = new ImageView(image);
            imageView.setFitWidth(150);
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent mouseEvent) {

                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){

                        if(mouseEvent.getClickCount() == 2){
                            try {
                                BorderPane borderPane = new BorderPane();
                                ImageView imageView = new ImageView();
                                Image image = new Image(new FileInputStream(imageFile));
                                imageView.setImage(image);
                                //imageView.setStyle("-fx-background-color: BLACK");
                                //imageView.setFitHeight(stage.getHeight() - 10);
                                imageView.setPreserveRatio(true);
                                imageView.setSmooth(true);
                                imageView.setCache(true);
                                borderPane.setCenter(imageView);
                                //borderPane.setStyle("-fx-background-color: BLACK");
                                Stage newStage = new Stage();
                                //newStage.setWidth(stage.getWidth());
                                //newStage.setHeight(stage.getHeight());
                                newStage.setTitle("image "+String.valueOf(i));
                                Scene scene = new Scene(borderPane, Color.BLACK);
                                newStage.setScene(scene);
                                newStage.show();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });
        }
        catch (FileNotFoundException ex) {
        ex.printStackTrace();
        }
        return imageView;
    }

}
