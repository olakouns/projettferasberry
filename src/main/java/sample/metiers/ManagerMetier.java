package sample.metiers;

import sample.serialCommunication.UsbController;

public class ManagerMetier implements ControlRobot {
    public  UsbController usbController = UsbController.getInstance();
    private static ManagerMetier managerMetier = new ManagerMetier();

    private ManagerMetier() {
        usbController.initilizer();
        if (usbController != null){
            usbController.read();
            System.out.println("call");
        }
        else {
            System.out.println("out");
        }
    }

    public  void stopreceiver(){
        usbController.close();
    }

    public static ManagerMetier getInstance() {
        return managerMetier;
    }

    @Override
    public void goForward() {
        usbController.send("0");
    }

    @Override
    public void goBack() {
        usbController.send("1");
    }

    @Override
    public void turnLeft() {
        usbController.send("3");
    }

    @Override
    public void turnRight() {
        usbController.send("2");
    }

    @Override
    public void stop() {
        usbController.send("4");
    }

    @Override
    public void ralentir() {
        usbController.send("5");
    }
}
