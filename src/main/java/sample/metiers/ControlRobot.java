package sample.metiers;

public interface ControlRobot {
    public void goForward();
    public void goBack();
    public void turnLeft();
    public void turnRight();
    public void stop();
    public void ralentir();

}
