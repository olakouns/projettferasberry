package sample.entities;

public class DataReceive {
    private float ultfront;
    private float ultback;
    private float ultleft;
    private float ultright;
    private float humidite;
    private float temps;
    private float flamme;
    private float gaz;
    public DataReceive() {
    }

    public DataReceive(float ultfront, float ultback, float ultleft, float ultright, float humidite, float temps, float flamme, float gaz) {
        this.ultfront = ultfront;
        this.ultback = ultback;
        this.ultleft = ultleft;
        this.ultright = ultright;
        this.humidite = humidite;
        this.temps = temps;
        this.flamme = flamme;
        this.gaz = gaz;
    }

    public float getUltfront() {
        return ultfront;
    }

    public void setUltfront(float ultfront) {
        this.ultfront = ultfront;
    }

    public float getUltback() {
        return ultback;
    }

    public void setUltback(float ultback) {
        this.ultback = ultback;
    }

    public float getUltleft() {
        return ultleft;
    }

    public void setUltleft(float ultleft) {
        this.ultleft = ultleft;
    }

    public float getUltright() {
        return ultright;
    }

    public void setUltright(float ultright) {
        this.ultright = ultright;
    }

    @Override
    public String toString() {
        return "DataReceive{" +
                "ultfront=" + ultfront +
                ", ultback=" + ultback +
                ", ultleft=" + ultleft +
                ", ultright=" + ultright +
                ", humidite=" + humidite +
                ", temps=" + temps +
                ", flamme=" + flamme +
                ", gaz=" + gaz +
                '}';
    }
}
