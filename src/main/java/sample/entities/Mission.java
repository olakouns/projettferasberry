package sample.entities;

import java.util.ArrayList;

public class Mission {
    private int id;
    private double distance;
    private  int nbrobjectdetect;
    private String duree;
    private ArrayList<ObjectDetect> objectDetects;

//    public Mission(ArrayList<ObjectDetect> objectDetects) {
//        this.objectDetects = objectDetects;
//    }

    public Mission() {
    }



    public Mission(double distance, int nbrobjectdetect, String duree) {
        this.distance = distance;
        this.nbrobjectdetect = nbrobjectdetect;
        this.duree = duree;
    }

    public Mission(int id, double distance, int nbrobjectdetect, String duree) {
        this.id = id;
        this.distance = distance;
        this.nbrobjectdetect = nbrobjectdetect;
        this.duree = duree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getNbrobjectdetect() {
        return nbrobjectdetect;
    }

    public void setNbrobjectdetect(int nbrobjectdetect) {
        this.nbrobjectdetect = nbrobjectdetect;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }


    public ArrayList<ObjectDetect> getObjectDetects() {
        return objectDetects;
    }

    public void setObjectDetects(ArrayList<ObjectDetect> objectDetects) {
        this.objectDetects = objectDetects;
    }
}
