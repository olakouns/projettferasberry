package sample.entities;

public class ObjectDetect {
    private int id;
    private double distance;
    private String typeofobject;
    private Integer time;
    private Integer missionid;


    public ObjectDetect() {
    }
    public ObjectDetect(double distance, String typeofobject, Integer time) {
        this.distance = distance;
        this.typeofobject = typeofobject;
        this.time = time;
    }
    public ObjectDetect(int id, double distance, String typeofobject, Integer time) {
        this.id = id;
        this.distance = distance;
        this.typeofobject = typeofobject;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTypeofobject() {
        return typeofobject;
    }

    public void setTypeofobject(String typeofobject) {
        this.typeofobject = typeofobject;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
    public Integer getMissionid() {
        return missionid;
    }

    public void setMissionid(Integer missionid) {
        this.missionid = missionid;
    }
}
