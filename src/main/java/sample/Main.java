package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/home.fxml"));
        primaryStage.setTitle("Home");
       // primaryStage.getIcons().add(new Image("/images/plus.png"));
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 615, 683));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
