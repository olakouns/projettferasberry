package sample.dao;

import sample.entities.Mission;
import sample.entities.ObjectDetect;

import java.sql.SQLException;
import java.util.List;

public interface DBManager {
    void connect();
    void close();

    List<Mission> getAllMission() throws SQLException;
    Mission getMissionById(int id) throws SQLException;
    Mission insertMission(Mission mission) throws  SQLException;
    Mission lassMission() throws  SQLException;
    Mission updateMission(Mission mission) throws SQLException;
    void delteMission(int idmission) throws  SQLException;

    List<ObjectDetect> getAllObjectfindbyMission(int idMission) throws SQLException;
    ObjectDetect insertObjectDetect(int idMission, ObjectDetect objectDetect) throws SQLException;
    void deleteObjectDetect(int idobjectDetect) throws SQLException;

}
