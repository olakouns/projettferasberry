package sample.dao;


import sample.entities.Mission;
import sample.entities.ObjectDetect;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManangerImpl implements DBManager{
    private static DBManangerImpl dbManagerImp;
    private static String DATABASE = "objectdetect-app.db";
    private Connection connection;
    private Statement statement;

    private DBManangerImpl() {
    }

    public static DBManangerImpl getInstance() {
        if(dbManagerImp==null) {
            dbManagerImp = new DBManangerImpl();
            dbManagerImp.connect();
        }

        return dbManagerImp;
    }

    @Override
    public void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DATABASE);
            statement = connection.createStatement();

            System.out.println("Connexion a " + DATABASE + " avec succès");
        } catch (ClassNotFoundException | SQLException notFoundException) {
            notFoundException.printStackTrace();
            System.out.println("Erreur de connecxion");
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Mission> getAllMission() throws SQLException {
        String sql = "SELECT * FROM missions;";
        ResultSet resultSet = statement.executeQuery(sql);
        List<Mission> missions = new ArrayList<>();
        while (resultSet.next()){
            Mission mission = new Mission();
              mission.setId(resultSet.getInt("id"));
              mission.setDistance(resultSet.getDouble("distance"));
              mission.setNbrobjectdetect(resultSet.getInt("nbrobjdetect"));
              mission.setDuree(resultSet.getString("duree"));
              missions.add(mission);
        }

        for (Mission mission :
                missions) {
            mission.setObjectDetects((ArrayList<ObjectDetect>) getAllObjectfindbyMission(mission.getId()));
        }
        return missions;


    }

    @Override
    public Mission getMissionById(int id) throws SQLException {
        String sql = "SELECT * FROM missions WHERE id = "+ id +";";
        ResultSet resultSet = statement.executeQuery(sql);
        Mission mission = new Mission();
        mission.setId(resultSet.getInt("id"));
        mission.setDistance(resultSet.getDouble("distance"));
        mission.setNbrobjectdetect(resultSet.getInt("nbrobjdetect"));
        mission.setDuree(resultSet.getString("duree"));
        return mission;
    }

    @Override
    public Mission lassMission() throws SQLException {
        String sql = "SELECT * FROM missions ORDER BY id DESC LIMIT 1;";
        ResultSet resultSet = statement.executeQuery(sql);
        Mission mission = new Mission();
        mission.setId(resultSet.getInt("id"));
        mission.setDistance(resultSet.getDouble("distance"));
        mission.setNbrobjectdetect(resultSet.getInt("nbrobjdetect"));
        mission.setDuree(resultSet.getString("duree"));
        return mission;
    }



    @Override
    public Mission insertMission(Mission mission) throws SQLException {
        String sql = "INSERT INTO missions(distance, nbrobjdetect, duree) values (?, ?, ?)";
        PreparedStatement preparedStmt = connection.prepareStatement(sql);
        preparedStmt.setDouble (1, mission.getDistance());
        preparedStmt.setInt (2, mission.getNbrobjectdetect());
        preparedStmt.setString (3,  mission.getDuree());
        int row = preparedStmt.executeUpdate();
        if(row==0){
            return null;
        }
        try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                mission.setId(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating menu failed, no ID obtained.");
            }
        }
        return mission;
    }

    @Override
    public Mission updateMission(Mission mission) throws SQLException {
        String sql = "UPDATE missions SET distance=?, nbrobjdetect=?, duree=? WHERE id=?; ";
        PreparedStatement preparedStmt = connection.prepareStatement(sql);
        preparedStmt.setDouble (1, mission.getDistance());
        preparedStmt.setInt (2, mission.getNbrobjectdetect());
        preparedStmt.setString (3, mission.getDuree());
        preparedStmt.setInt (4, (int)mission.getId());
        int row = preparedStmt.executeUpdate();
        if(row==0){
            return null;
        }

        return mission;
    }


    @Override
    public void delteMission(int idmission) throws SQLException {
        String sql = "DELETE FROM missions WHERE id=?; ";
        PreparedStatement preparedStmt = connection.prepareStatement(sql);
        preparedStmt.setInt(1, idmission);
        preparedStmt.executeUpdate();
    }

    @Override
    public List<ObjectDetect> getAllObjectfindbyMission(int idMission) throws SQLException {
        String sql = "SELECT * FROM objectdetect WHERE id_mission = " +idMission + ";";
        ResultSet resultSet = statement.executeQuery(sql);
        List<ObjectDetect> objectDetects = new ArrayList<>();
        while (resultSet.next()){
            ObjectDetect objectDetect = new ObjectDetect();
            objectDetect.setId(resultSet.getInt("id"));
            objectDetect.setDistance(resultSet.getDouble("distance"));
            objectDetect.setTypeofobject(resultSet.getString("typeofobject"));
            objectDetect.setTime(resultSet.getInt("time"));
            objectDetects.add(objectDetect);
        }
        return objectDetects;
    }

    @Override
    public ObjectDetect insertObjectDetect(int idMission, ObjectDetect objectDetect) throws SQLException {
        String sql = "INSERT INTO objectdetect(distance, typeofobject, time, id_mission) values (?, ?, ?, ?)";
        PreparedStatement preparedStmt = connection.prepareStatement(sql);
        preparedStmt.setDouble (1, objectDetect.getDistance());
        preparedStmt.setString (2, objectDetect.getTypeofobject());
        preparedStmt.setInt (3, objectDetect.getTime());
        preparedStmt.setInt (4, idMission);
        int row = preparedStmt.executeUpdate();
        if(row==0){
            return null;
        }
        try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                objectDetect.setId(generatedKeys.getInt(1));
                objectDetect.setMissionid(idMission);
            }
            else {
                throw new SQLException("Creating menu failed, no ID obtained.");
            }
        }
        return objectDetect;
    }

    @Override
    public void deleteObjectDetect(int  idobjectDetect) throws SQLException {
        String sql = "DELETE FROM objectdetect WHERE id=?; ";
        PreparedStatement preparedStmt = connection.prepareStatement(sql);
        preparedStmt.setInt(1, idobjectDetect);
        preparedStmt.executeUpdate();
    }
}

