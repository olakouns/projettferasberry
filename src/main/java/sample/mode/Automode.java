package sample.mode;

import com.google.gson.Gson;
import javafx.application.Platform;
import sample.controller.DashbordController;
import sample.entities.DataReceive;
import sample.machineLearning.ObjectDetectionPy;
import sample.metiers.ManagerMetier;
import sample.serialCommunication.Usbcallback;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Automode implements Usbcallback {
    private Gson gson = new Gson();
    private DataReceive dataReceive;
    private float distance;
    private float disatancedetect;
    private  ManagerMetier managerMetier = ManagerMetier.getInstance();
    private HashMap<String, Integer> map;
    public Automode(float distance) {
        this.distance = distance;
    }

    @Override
    public synchronized void receiveData(String data) throws InterruptedException {
        if(data.length()>=94){
            String lettre=data.substring(0,1);
            if(lettre.equals("{")){
                dataReceive = gson.fromJson(data, DataReceive.class);
                System.out.println(dataReceive);
                if (dataReceive.getUltfront() <=20){
                    disatancedetect = dataReceive.getUltfront();
                    System.out.println("Detection d'obstacle frontal");
                    if (dataReceive.getUltright() <= 15){
                        System.out.println("detection d'obstacle a droite");
                        //delay(500);
                        if (dataReceive.getUltleft() <= 15){
                            System.out.println("Detection d'obstacle a gauche");
                            //delay(500);
                            managerMetier.goBack();
                            // faire demi tour
                        }
                        else {
                            System.out.println("tourne a gauche");
                            //delay(800);
                            managerMetier.turnLeft();
    //                     delay(1500);
                        }
                    }
                    else {
                        System.out.println("tourne a droite");
                        managerMetier.turnRight();
                        //delay(800);
    //                managerMetier.stop();  optionnel
    //                delay(1500);
                    }
                }
                else {
                    System.out.println("Pas d'obstacle frontal");
                    //delay(500);
                    managerMetier.goForward();
                }
    //
            }
        }
    }
    public float getdistancefront(){
        return disatancedetect;
    }
    private  void delay(long value){
        try {
            Thread.sleep(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void load(){

        Date date = new Date();
        System.out.println("load of automode");
        new Thread(new Runnable() {
            @Override
            public void run() {
                map = (HashMap<String, Integer>) ObjectDetectionPy.detectObject(DashbordController.cameraController.takesanpshut(),DashbordController.mission.getId());
                System.out.println(map);
                DashbordController.dashbordController.setmap(map);
                System.out.println("ok");
                map.forEach((type, nbre) -> {
                    Platform.runLater(() -> DashbordController.dashbordController.loadashbordItem(type,disatancedetect,Integer.parseInt(nbre.toString()),date));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                });
            }
        }).start();
    }
}
