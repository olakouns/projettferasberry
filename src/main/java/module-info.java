module rasberry {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires opencv;
    requires com.fazecast.jSerialComm;
    requires sqlite.jdbc;
    requires gson;
    opens sample.controller to javafx.fxml;
    exports sample;
}